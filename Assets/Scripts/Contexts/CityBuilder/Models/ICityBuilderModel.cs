﻿using System.Collections.Generic;

namespace Company.Contexts.CityBuilder
{
    public interface ICityBuilderModel
    {
        List<IBuilding> Buildings { get; set; }

        void Reset();
    }
}