﻿using System.Collections.Generic;

namespace Company.Contexts.CityBuilder
{
    public interface IBuilding
    {
        string Id { get; set; }
        string Name { get; set; }
        
        string Description { get; set; }
        
        IEnumerable<string> RequirementIds { get; set; }
        bool IsBuilded { get; set; }
    }
}