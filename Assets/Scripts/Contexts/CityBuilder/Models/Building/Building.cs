﻿using System.Collections.Generic;

namespace Company.Contexts.CityBuilder
{
    public class Building : IBuilding
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        
        public IEnumerable<string> RequirementIds { get; set; }
        public bool IsBuilded { get; set; }

        public Building(string id, string name, string description, IEnumerable<string> requirementIds, bool isBuilded)
        {
            Id = id;
            Name = name;
            Description = description;
            
            RequirementIds = requirementIds;
            IsBuilded = isBuilded;
        }
    }
}