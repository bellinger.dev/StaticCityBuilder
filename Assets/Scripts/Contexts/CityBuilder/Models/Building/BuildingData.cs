﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    [Serializable]
    public class BuildingData
    {
        [SerializeField] private string _id;
        [SerializeField] private string _name;
        [SerializeField] private string _description;
        [SerializeField] private bool _isBuilded;
        
        [SerializeField] private List<string> _requirementBuildingIds;
        
        public string Id
        {
            get { return _id; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string Description
        {
            get { return _description; }
        }

        public bool IsBuilded
        {
            get { return _isBuilded; }
        }

        public IEnumerable<string> RequirementIds
        {
            get { return _requirementBuildingIds; }
        }
    }
}