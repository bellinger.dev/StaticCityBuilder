﻿using System;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    [Serializable]
    public class BuildingProgressData
    {
        [SerializeField] private string _id;
        [SerializeField] private bool _isBuilded;

        public BuildingProgressData(string id, bool isBuilded)
        {
            _id = id;
            _isBuilded = isBuilded;
        }
        
        public string Id
        {
            get { return _id; }
        }

        public bool IsBuilded
        {
            get { return _isBuilded; }
            set { _isBuilded = value; }
        }
    }
}