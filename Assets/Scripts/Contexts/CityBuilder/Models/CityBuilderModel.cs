﻿using System.Collections.Generic;

namespace Company.Contexts.CityBuilder
{
    public class CityBuilderModel : ICityBuilderModel
    {
        public List<IBuilding> Buildings { get; set; }
        
        public void Reset()
        {
            Buildings = new List<IBuilding>();
        }
    }
}