﻿using strange.extensions.context.impl;

namespace Company.Contexts.CityBuilder
{
    public class CityBuilderRoot : ContextView
    {
        void Awake()
        {
            context = new CityBuilderContext(this);
        }
    }
}