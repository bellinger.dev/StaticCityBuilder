﻿using strange.extensions.command.api;
using strange.extensions.command.impl;
using strange.extensions.context.api;
using strange.extensions.context.impl;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
	public class CityBuilderContext : MVCSContext
	{
		public CityBuilderContext (MonoBehaviour view) : base(view)
		{
		}

		public CityBuilderContext (MonoBehaviour view, ContextStartupFlags flags) : base(view, flags)
		{
		}
		
		protected override void addCoreComponents()
		{
			base.addCoreComponents();
			injectionBinder.Unbind<ICommandBinder>();
			injectionBinder.Bind<ICommandBinder>().To<SignalCommandBinder>().ToSingleton();
		}
		
		public override IContext Start()
		{
			base.Start();
			
			InitSignal initSignal = (InitSignal) injectionBinder.GetInstance<InitSignal>();
			initSignal.Dispatch();
			
			return this;
		}

		protected override void mapBindings()
		{
			BindInjections();
			BindMediations();
			BindCommands();
		}

		private void BindInjections()
		{
			injectionBinder.Bind<ICityBuilderModel>().To<CityBuilderModel>()
				.ToSingleton()
				.CrossContext();
			
			injectionBinder.Bind<IProgressRepository>().To<ProgressRepository>()
				.ToSingleton();

			injectionBinder.Bind<IBuildingsRepository>()
				.To(GameObject.FindObjectOfType<BuildingsRepository>())
				.ToSingleton();

			injectionBinder.Bind<IProgressDatabase>().To<PlayerPrefsProgressDatabase>()
				.ToSingleton();
			
			injectionBinder.Bind<IGameLoopService>().To(GameObject.FindObjectOfType<GameLoopService>())
				.ToSingleton();

#if (UNITY_EDITOR || UNITY_STANDALONE || UNITY_STANDALONE_OSX)
			injectionBinder.Bind<IInputService>().To<EditorInputService>().ToSingleton();
#else
			injectionBinder.Bind<IInputService>().To<MobileInputService>().ToSingleton();
			#endif
			
			injectionBinder.Bind<Camera>().To(GameObject.FindGameObjectWithTag("CityBuilderCamera").GetComponent<Camera>())
				.ToSingleton()
				.ToName("CityBuilderCamera");
		}

		private void BindMediations()
		{
			mediationBinder.Bind<BuildingView>().To<BuildingMediator>();
		
			mediationBinder.Bind<AvailableBuildingsView>().To<AvailableBuildingsMediator>();
			mediationBinder.Bind<BuildRequirementsView>().To<BuildRequirementsMediator>();
			mediationBinder.Bind<BuildConfirmationView>().To<BuildConfirmationMediator>();


			mediationBinder.Bind<GameLoopService>();
		}

		private void BindCommands()
		{
			commandBinder.Bind<InitSignal>().To<InitCommand>();
			commandBinder.Bind<DeInitSignal>().To<DeInitCommand>();

			commandBinder.Bind<GetBuildingsSignal>().To<CityBuilderGetBuildingsCommand>();
			commandBinder.Bind<GetProgressSignal>().To<CityBuilderGetBuildingsProgressCommand>();

			commandBinder.Bind<CheckBuildingAvailabilitySignal>().To<CheckBuildingAvailabilityCommand>();

			commandBinder.Bind<BuildingsUpdatedSignal>();
			commandBinder.Bind<ProgressUpdatedSignal>();

			commandBinder.Bind<InputTouchDownSignal>().To<RaycastBuildingCommand>();
			commandBinder.Bind<InputTouchUpSignal>();

			commandBinder.Bind<BuildRequestedSignal>();
			commandBinder.Bind<BuildBuildingRefusedSignal>();

			commandBinder.Bind<BuildBuildingConfirmedSignal>().To<BuildBuildingCommand>();

			commandBinder.Bind<ActivateBuildingSignal>();

			commandBinder.Bind<GameLoopUpdateSignal>();
			commandBinder.Bind<GameLoopFixedUpdateSignal>();
		}
	}
}