﻿namespace Company.Contexts.CityBuilder
{
    public interface IInputService
    {
        InputTouchDownSignal TouchDownSignal { get; set; }
        InputTouchUpSignal TouchUpSignal { get; set; }

        void Init();
        void DeInit();
    }
}