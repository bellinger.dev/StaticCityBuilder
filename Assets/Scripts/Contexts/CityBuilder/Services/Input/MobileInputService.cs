﻿using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class MobileInputService : IInputService
    {
        [Inject]
        public InputTouchDownSignal TouchDownSignal { get; set; }
        
        [Inject]
        public InputTouchUpSignal TouchUpSignal { get; set; }
        
        [Inject]
        public GameLoopUpdateSignal UpdateSignal { get; set; }
        
        public void Init()
        {
            UpdateSignal.AddListener(HandleInputEvents);
        }

        public void DeInit()
        {
            UpdateSignal.RemoveListener(HandleInputEvents);
        }

        private void HandleInputEvents()
        {
            if (Input.touchCount > 0)
            {
                Touch touch = Input.GetTouch(0);
                
                if (touch.phase == TouchPhase.Began)
                    TouchDownSignal.Dispatch(touch.position);
                    
                if (touch.phase == TouchPhase.Ended)
                    TouchUpSignal.Dispatch(touch.position);
            }
        }
    }
}