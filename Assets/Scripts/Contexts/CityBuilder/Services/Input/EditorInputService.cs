﻿using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class EditorInputService : IInputService
    {
        [Inject]
        public InputTouchDownSignal TouchDownSignal { get; set; }
        
        [Inject]
        public InputTouchUpSignal TouchUpSignal { get; set; }

        [Inject]
        public GameLoopUpdateSignal UpdateSignal { get; set; }
        
        public void Init()
        {
            UpdateSignal.AddListener(HandleInputEvents);
        }

        public void DeInit()
        {
            UpdateSignal.RemoveListener(HandleInputEvents);
        }

        private void HandleInputEvents()
        {
            if (Input.GetMouseButtonDown(0))
                TouchDownSignal.Dispatch(Input.mousePosition);
            
            if (Input.GetMouseButtonUp(0))
                TouchUpSignal.Dispatch(Input.mousePosition);
        }
    }
}