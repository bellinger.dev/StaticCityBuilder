﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class PlayerPrefsProgressDatabase : IProgressDatabase
    {
        [Serializable]
        private class JsonContainer
        {
            [SerializeField] private BuildingProgressData[] array;

            public BuildingProgressData[] Array
            {
                get { return array; }
            }

            public JsonContainer(BuildingProgressData[] array)
            {
                this.array = array;
            }
        }
        
        private const string DB_PREFS_ID = "BuildingProgressDatabasePrefsID";

        private List<BuildingProgressData> _progressData = new List<BuildingProgressData>();
        public IEnumerable<BuildingProgressData> ProgressData
        {
            get { return _progressData; }
        }

        public bool IsInit { get; private set; }

        public void Init()
        {   
            if (PlayerPrefs.HasKey(DB_PREFS_ID))
            {
                string json = PlayerPrefs.GetString(DB_PREFS_ID);
                
                _progressData = new List<BuildingProgressData>(JsonUtility.FromJson<JsonContainer>(json).Array);
            }

            IsInit = true;
        }

        public void DeInit()
        {
            Commit();

            _progressData = null;
            

            IsInit = false;
        }

        public BuildingProgressData GetBuidingProgress(string id)
        {
            return _progressData.SingleOrDefault(pd => pd.Id == id);
        }

        public void SetBuildingProgress(BuildingProgressData newProgressData)
        {
            BuildingProgressData oldProgressData = _progressData.SingleOrDefault(pd => pd.Id == newProgressData.Id);
            if (oldProgressData != null)
                oldProgressData.IsBuilded = newProgressData.IsBuilded;
            else
                _progressData.Add(newProgressData);
            
            Commit();
        }

        public void Commit()
        {
            string json = JsonUtility.ToJson(new JsonContainer(_progressData.ToArray()));
                
            PlayerPrefs.SetString(DB_PREFS_ID, json);
            
            PlayerPrefs.Save();
        }
    }
}