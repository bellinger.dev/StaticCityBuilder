﻿namespace Company.Contexts.CityBuilder
{
    public interface IProgressRepository
    {
        ProgressReceivedSignal ProgressReceivedSignal { get; set; }
        
        bool IsInit { get; }

        void Init();
        void DeInit();
        
        void GetProgress();
        void SetProgress(BuildingProgressData progressData);
    }
}