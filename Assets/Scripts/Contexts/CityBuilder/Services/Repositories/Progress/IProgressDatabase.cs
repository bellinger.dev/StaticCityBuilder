﻿using System.Collections.Generic;

namespace Company.Contexts.CityBuilder
{
    public interface IProgressDatabase
    {
        IEnumerable<BuildingProgressData> ProgressData { get; }

        bool IsInit { get; }
        
        void Init();
        void DeInit();
        
        BuildingProgressData GetBuidingProgress(string id);
        void SetBuildingProgress(BuildingProgressData progressData);

        void Commit();
    }
}