﻿namespace Company.Contexts.CityBuilder
{
    public class ProgressRepository : IProgressRepository
    {
        public ProgressReceivedSignal ProgressReceivedSignal { get; set; }

        [Inject]
        public IProgressDatabase Database { get; set; }
        
        public bool IsInit { get; private set; }
        
        public void Init()
        {
            ProgressReceivedSignal = new ProgressReceivedSignal();
            
            if (!Database.IsInit)
                Database.Init();

            IsInit = true;
        }

        public void DeInit()
        {
            ProgressReceivedSignal = null;
            
            if (Database.IsInit)
                Database.DeInit();
            
            IsInit = false;
        }
        
        public void GetProgress()
        {
            ProgressReceivedSignal.Dispatch(Database.ProgressData);
        }

        public void SetProgress(BuildingProgressData progressData)
        {
            Database.SetBuildingProgress(progressData);
        }
    }
}