﻿using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class BuildingsRepository : MonoBehaviour, IBuildingsRepository
    {
        public BuildingsReceivedSignal BuildingsReceivedSignal { get; set; }

        [SerializeField] private ScriptableObjectBuildingDatabase _database;

        public bool IsInit { get; private set; }
        
        public void Init()
        {
            BuildingsReceivedSignal = new BuildingsReceivedSignal();

            IsInit = true;
        }

        public void DeInit()
        {
            BuildingsReceivedSignal = null;
            
            IsInit = false;
        }
        
        public void GetBuildings()
        {
            BuildingsReceivedSignal.Dispatch(_database.BuildingsData);
        }
    }
}