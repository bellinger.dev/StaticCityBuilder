﻿namespace Company.Contexts.CityBuilder
{
    public interface IBuildingsRepository
    {
        BuildingsReceivedSignal BuildingsReceivedSignal { get; set; }
        
        bool IsInit { get; }
        
        void Init();
        void DeInit();
        
        void GetBuildings();
    }
}