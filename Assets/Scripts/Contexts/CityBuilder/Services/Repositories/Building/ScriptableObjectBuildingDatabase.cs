﻿using System.Collections.Generic;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    [CreateAssetMenu(menuName = "Data/Repositories/BuildingDatabase", fileName = "SO_BuildingRepository_01")]
    public class ScriptableObjectBuildingDatabase : ScriptableObject
    {
        [SerializeField] private List<BuildingData> _buildingsData;

        public IEnumerable<BuildingData> BuildingsData
        {
            get { return _buildingsData; }
        }
    }
}