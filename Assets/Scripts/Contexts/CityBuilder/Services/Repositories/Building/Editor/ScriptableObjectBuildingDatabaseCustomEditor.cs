﻿using System.Collections.Generic;
using Company.Contexts.CityBuilder;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ScriptableObjectBuildingDatabase))]
public class ScriptableObjectBuildingDatabaseCustomEditor : Editor
{
	public override void OnInspectorGUI()
	{
		serializedObject.Update();
		
		base.OnInspectorGUI();
		
		GUILayout.BeginVertical();
		{
			if (GUILayout.Button("FindBuildings"))
			{
				FindBuildings(serializedObject.FindProperty("_buildingsData"));
			}
		}
		GUILayout.EndVertical();

		serializedObject.ApplyModifiedProperties();
	}

	private void FindBuildings(SerializedProperty dataArray)
	{
		List<BuildingView> views = new List<BuildingView>(FindObjectsOfType<BuildingView>());

		for (int i = 0; i < dataArray.arraySize; i++)
		{
			SerializedProperty dataItem = dataArray.GetArrayElementAtIndex(i).FindPropertyRelative("_id");
			
			if (dataItem != null)
				views.RemoveAll(v => v.Id == dataItem.stringValue);
		}
		
		foreach (var view in views)
		{
			int insertionIndex = dataArray.arraySize;
			dataArray.InsertArrayElementAtIndex(insertionIndex);

			SerializedProperty dataItem = dataArray.GetArrayElementAtIndex(insertionIndex);

			dataItem.FindPropertyRelative("_id").stringValue = view.Id;
			dataItem.FindPropertyRelative("_name").stringValue = view.Name;
		}
	}
}
