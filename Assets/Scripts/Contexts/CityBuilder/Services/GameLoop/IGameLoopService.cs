﻿namespace Company.Contexts.CityBuilder
{
    public interface IGameLoopService
    {
        GameLoopUpdateSignal UpdateSignal { get; set; }
        GameLoopFixedUpdateSignal FixedUpdateSignal { get; set; }
        
        bool IsRunning { get; set; }
    }
}