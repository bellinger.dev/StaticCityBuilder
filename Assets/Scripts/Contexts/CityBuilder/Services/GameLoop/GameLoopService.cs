﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class GameLoopService : View, IGameLoopService
    {
        [Inject]
        public GameLoopUpdateSignal UpdateSignal { get; set; }
        
        [Inject]
        public GameLoopFixedUpdateSignal FixedUpdateSignal { get; set; }
        
        public bool IsRunning { get; set; }

        private void Update()
        {
            if (IsRunning)
                if (UpdateSignal != null)
                    UpdateSignal.Dispatch();
        }

        private void FixedUpdate()
        {
            if (IsRunning)
                if (FixedUpdateSignal != null)
                    FixedUpdateSignal.Dispatch();
        }
    }
}