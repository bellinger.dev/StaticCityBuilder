﻿using strange.extensions.command.impl;

namespace Company.Contexts.CityBuilder
{
	public class InitCommand : Command
	{
		[Inject]
		public GetBuildingsSignal GetBuildingsSignal { get; set; }
		
		[Inject]
		public GetProgressSignal GetProgressSignal { get; set; }
		
		[Inject]
		public BuildingsUpdatedSignal BuildingsUpdatedSignal { get; set; }
		
		[Inject]
		public ProgressUpdatedSignal ProgressUpdatedSignal { get; set; }
		
		[Inject]
		public IGameLoopService GameLoopService { get; set; }
		
		[Inject]
		public IInputService InputService { get; set; }
		
		[Inject]
		public ICityBuilderModel Model { get; set; }
		
		public override void Execute()
		{
			Retain();
		
			Model.Reset();
			
			InputService.Init();
			GameLoopService.IsRunning = true;
			
			GetBuildings();
		}

		private void GetBuildings()
		{
			BuildingsUpdatedSignal.AddOnce(GetProgress);
			GetBuildingsSignal.Dispatch();
		}

		private void GetProgress()
		{
			ProgressUpdatedSignal.AddOnce(ReleaseCommand);
			GetProgressSignal.Dispatch();
		}

		private void ReleaseCommand()
		{
			Release();
		}
	}
}