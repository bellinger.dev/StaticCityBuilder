﻿using strange.extensions.command.impl;

namespace Company.Contexts.CityBuilder
{
    public class DeInitCommand : Command
    {
        [Inject]
        public ICityBuilderModel CityBuilderModel { get; set; }
        
        [Inject]
        public IGameLoopService GameLoopService { get; set; }
		
        [Inject]
        public IInputService InputService { get; set; }

        public override void Execute()
        {
            CityBuilderModel.Reset();

            InputService.DeInit();
            GameLoopService.IsRunning = true;
        }
    }
}