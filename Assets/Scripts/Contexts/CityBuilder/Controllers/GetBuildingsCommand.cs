﻿using System.Collections.Generic;
using strange.extensions.command.impl;

namespace Company.Contexts.CityBuilder
{   
    public class CityBuilderGetBuildingsCommand : Command
    {
        [Inject]
        public IBuildingsRepository Repository { get; set; }
        
        [Inject]
        public ICityBuilderModel Model { get; set; }

        [Inject]
        public BuildingsUpdatedSignal BuildingsUpdatedSignal { get; set; }
        
        public override void Execute()
        {
            Retain();
            
            if (!Repository.IsInit)
                Repository.Init();
            
            Repository.BuildingsReceivedSignal.AddOnce(AddBuildingsToModel);   
            Repository.GetBuildings();
        }

        private void AddBuildingsToModel(IEnumerable<BuildingData> buildingsData)
        {
            foreach (BuildingData building in buildingsData)
            {
                Model.Buildings.Add(new Building(
                    building.Id,
                    building.Name,
                    building.Description,
                    building.RequirementIds, 
                    building.IsBuilded));
            }
            
            BuildingsUpdatedSignal.Dispatch();
            
            Release();
        }
    }
}