﻿using System.Collections.Generic;
using System.Linq;
using strange.extensions.command.impl;

namespace Company.Contexts.CityBuilder
{
    public class CityBuilderGetBuildingsProgressCommand : Command
    {
        [Inject]
        public IProgressRepository Repository { get; set; }
        
        [Inject]
        public ICityBuilderModel Model { get; set; }

        [Inject]
        public ProgressUpdatedSignal ProgressUpdatedSignal { get; set; }

        public override void Execute()
        {
            Retain();
            
            if (!Repository.IsInit)
                Repository.Init();
            
            Repository.ProgressReceivedSignal.AddOnce(ApplyProgressOnBuildings);   
            Repository.GetProgress();   
        }

        private void ApplyProgressOnBuildings(IEnumerable<BuildingProgressData> buildings)
        {
            if (buildings != null)
            {
                foreach (var building in Model.Buildings)
                {
                    BuildingProgressData progress = buildings.SingleOrDefault(b => b.Id == building.Id);
                    if (progress != null)
                        building.IsBuilded = progress.IsBuilded;
                }
            }
            
            ProgressUpdatedSignal.Dispatch();
            
            Release();
        }
    }
}