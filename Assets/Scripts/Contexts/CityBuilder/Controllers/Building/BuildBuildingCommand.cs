﻿using System.Linq;
using strange.extensions.command.impl;

namespace Company.Contexts.CityBuilder
{
    public class BuildBuildingCommand : Command
    {
        [Inject]
        public string Id { get; set; }
        
        [Inject]
        public ICityBuilderModel Model { get; set; }
        
        [Inject]
        public IProgressRepository Repository { get; set; }
        
        [Inject]
        public BuildingsUpdatedSignal BuildingsUpdatedSignal { get; set; }
        
        public override void Execute()
        {
            IBuilding building = Model.Buildings.Single(b => b.Id == Id);

            building.IsBuilded = true;
            
            Repository.SetProgress(new BuildingProgressData(building.Id, building.IsBuilded));
            
            BuildingsUpdatedSignal.Dispatch();
        }
    }
}