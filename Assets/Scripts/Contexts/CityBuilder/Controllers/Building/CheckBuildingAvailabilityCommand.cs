﻿using System.Linq;
using strange.extensions.command.impl;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class CheckBuildingAvailabilityCommand : Command
    {
        [Inject]
        public string Id { get; set; }
        
        [Inject]
        public ICityBuilderModel Model { get; set; }
        
        [Inject]
        public BuildBuildingRefusedSignal RefusedSignal { get; set; }
        
        [Inject]
        public BuildBuildingConfirmedSignal ConfirmedSignal { get; set; }
        
        public override void Execute()
        {
            IBuilding building = Model.Buildings.Single(b => b.Id == Id);
            
            bool isRequirementsConfirmed = building.RequirementIds.All(ri => Model.Buildings.Any(b => b.Id == ri && b.IsBuilded));
            
            Debug.Log("Building Approve Status : " + isRequirementsConfirmed);
            
            if (isRequirementsConfirmed)
                ConfirmedSignal.Dispatch(Id);
            else
                RefusedSignal.Dispatch(Id);
        }
    }
}