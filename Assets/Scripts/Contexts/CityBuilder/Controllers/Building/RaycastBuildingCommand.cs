﻿using strange.extensions.command.impl;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class RaycastBuildingCommand : Command
    {
        [Inject] 
        public Vector2 TouchPosition { get; set; }
        
        [Inject("CityBuilderCamera")]
        public Camera Camera { get; set; }
        
        [Inject]
        public ActivateBuildingSignal ActivateBuildingSignal { get; set; }

        public override void Execute()
        {
            Debug.Log("Raycast Start");
            
            Vector3 origin = new Vector3(TouchPosition.x, TouchPosition.y, 1000);
            Ray ray = Camera.ScreenPointToRay(origin);

            RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);
            if (hit.collider != null)
            {
                Debug.Log("Raycast Hit");
                
                IBuildingView buildingView = hit.collider.GetComponentInParent<IBuildingView>();
                if (buildingView != null)
                    ActivateBuildingSignal.Dispatch(buildingView.Id);
            }
        }
    }
}