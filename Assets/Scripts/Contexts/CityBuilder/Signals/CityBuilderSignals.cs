﻿using System.Collections.Generic;
using strange.extensions.signal.impl;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class InitSignal : Signal {}
    public class DeInitSignal : Signal {}
    
    public class GetBuildingsSignal : Signal {}
    public class BuildingsReceivedSignal : Signal<IEnumerable<BuildingData>> {}
    
    public class GetProgressSignal : Signal {}
    public class ProgressReceivedSignal : Signal<IEnumerable<BuildingProgressData>> {}
    
    public class BuildingsUpdatedSignal : Signal {}
    public class ProgressUpdatedSignal : Signal {}
    
    public class ActivateBuildingSignal : Signal<string> {}
    
    public class CheckBuildingAvailabilitySignal : Signal<string> {}
    
    public class BuildBuildingConfirmedSignal : Signal<string> {}
    public class BuildBuildingRefusedSignal : Signal<string> {}
    
    public class BuildRequestedSignal : Signal<string> {}
        
    public class BuildConfirmedSignal : Signal<string> {}
    public class BuildCanceledSignal : Signal {}
    
    public class CloseSignal : Signal {}
    
    public class InputTouchDownSignal : Signal<Vector2> {}
    public class InputTouchUpSignal : Signal<Vector2> {}
    
    public class GameLoopUpdateSignal : Signal {}
    public class GameLoopFixedUpdateSignal : Signal {}
}