﻿using System.Collections.Generic;

namespace Company.Contexts.CityBuilder
{
    public interface IBuildRequirementsView
    {
        CloseSignal CloseSignal { get; }
        
        void Init(IEnumerable<string> requirements);
    }
}