﻿using System.Linq;
using strange.extensions.mediation.impl;

namespace Company.Contexts.CityBuilder
{
    public class BuildRequirementsMediator : Mediator
    {
        [Inject]
        public BuildRequirementsView View { get; set; }
        
        [Inject]
        public ICityBuilderModel Model { get; set; }
        
        [Inject]
        public BuildBuildingRefusedSignal BuildBuildingRefusedSignal { get; set; }
        
        public override void OnRegister()
        {
            View.Init(null);
            View.CloseSignal.AddListener(Close);
            
            BuildBuildingRefusedSignal.AddListener(BindView);
        }

        public override void OnRemove()
        {
            View.CloseSignal.RemoveListener(Close);
            
            BuildBuildingRefusedSignal.RemoveListener(BindView);
        }

        private void BindView(string id)
        {
            IBuilding building = Model.Buildings.Single(b => b.Id == id);
            
            View.Init(building.RequirementIds);
            View.gameObject.SetActive(true);
        }

        private void Close()
        {
            gameObject.SetActive(false);
        }
    }
}