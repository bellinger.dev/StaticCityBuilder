﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Company.Contexts.CityBuilder
{
    public class BuildRequirementsView : View, IBuildRequirementsView
    {
        public CloseSignal CloseSignal { get; private set; }

        [SerializeField] private Text _requirementNames;

        public void Init(IEnumerable<string> requirements)
        {
            if (CloseSignal == null)
                CloseSignal = new CloseSignal();

            if (requirements == null)
            {
                _requirementNames.text = String.Empty;
                return;
            }
            
            StringBuilder builder = new StringBuilder();

            for (int i = 0; i < requirements.Count(); i++)
            {
                builder.Append(requirements.ElementAt(i));

                if (i < requirements.Count() - 1)
                    builder.Append(", ");
            }

            _requirementNames.text = builder.ToString();
        }

        public void OnCloseClicked()
        {
            CloseSignal.Dispatch();
        }
    }
}