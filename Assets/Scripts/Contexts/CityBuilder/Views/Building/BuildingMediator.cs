﻿using System.Linq;
using strange.extensions.mediation.impl;

namespace Company.Contexts.CityBuilder
{
    public class BuildingMediator : Mediator
    {
        [Inject]
        public BuildingView View { get; set; }
        
        [Inject]
        public ICityBuilderModel CityBuilderModel { get; set; }
        
        [Inject]
        public InitSignal InitSignal { get; set; }
        
        [Inject]
        public BuildingsUpdatedSignal BuildingsUpdatedSignal { get; set; }
        
        [Inject]
        public ProgressUpdatedSignal ProgressUpdatedSignal { get; set; }
        
        public override void OnRegister()
        {
            InitSignal.AddListener(OnInit);
            BuildingsUpdatedSignal.AddListener(UpdateBuildStatus);
            ProgressUpdatedSignal.AddListener(UpdateBuildStatus);
        }

        private void OnInit()
        {
            View.Init();
        }

        public override void OnRemove()
        {
            InitSignal.RemoveListener(OnInit);
            BuildingsUpdatedSignal.RemoveListener(UpdateBuildStatus);
            ProgressUpdatedSignal.RemoveListener(UpdateBuildStatus);
        }

        private void UpdateBuildStatus()
        {
            var building = CityBuilderModel.Buildings.Single(b => b.Id == View.Id);
            
            View.SetBuilded(building.IsBuilded);
        }
    }
}