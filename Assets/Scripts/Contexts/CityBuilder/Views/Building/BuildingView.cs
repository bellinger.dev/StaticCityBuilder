﻿using strange.extensions.mediation.impl;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
	public class BuildingView : View, IBuildingView
	{
		[SerializeField] private string _id;
		[SerializeField] private string _name;
		[SerializeField] private TextMesh _nameMesh;
		[SerializeField] private Renderer _renderer;
		[SerializeField] private bool _isBuilded;

		public string Id
		{
			get { return _id; }
		}
		
		public string Name
		{
			get { return _name; }
		}
		
		public void Init()
		{
			
		}

		public void SetBuilded(bool state)
		{
			_nameMesh.gameObject.SetActive(state);
			_renderer.enabled = state;
			
			_isBuilded = state;
		}

		private void OnValidate()
		{
			if (_nameMesh != null)
				_nameMesh.text = _name;

			if (_renderer != null) 
				_renderer.enabled = _isBuilded;

			if (_nameMesh != null && _nameMesh.gameObject.activeSelf != _isBuilded)
			_nameMesh.gameObject.SetActive(_isBuilded);
		}
	}
}