﻿namespace Company.Contexts.CityBuilder
{
    public interface IBuildingView
    {
        string Id { get; }
        string Name { get; }
        
        void Init();
        void SetBuilded(bool state);
    }
}