﻿using System.Collections.Generic;
using Company.Contexts.CityBuilder;

namespace Company.Contexts.CityBuilder
{
    public interface IAvailableBuildingsView
    {
        BuildRequestedSignal BuildRequestedSignal { get; }
        
        string AssociatedBuildingId { get; }
        
        void Init();

        void SetBuildings(IEnumerable<IBuilding> buildings);
    }
}