﻿using System.Collections.Generic;
using System.Linq;
using Company.Contexts.CityBuilder;
using strange.extensions.mediation.impl;
using UnityEngine;

namespace Company.Contexts.CityBuilder
{
    public class AvailableBuildingsView : View, IAvailableBuildingsView
    {
        [SerializeField] private GameObject _itemPrototype;
        [SerializeField] private Transform _itemsRoot;
        [SerializeField] private string _associatedBuildingId;

        private List<IBuildingItemView> _buildingViews;
        
        public BuildRequestedSignal BuildRequestedSignal { get; private set; }
        public CloseSignal CloseSignal { get; private set; }

        public string AssociatedBuildingId
        {
            get { return _associatedBuildingId; }
        }

        public void Init()
        {
            _buildingViews = new List<IBuildingItemView>();
            
            BuildRequestedSignal = new BuildRequestedSignal();
            CloseSignal = new CloseSignal();
        }

        public void SetBuildings(IEnumerable<IBuilding> buildings)
        {
            if (buildings == null) 
                SyncBuildinsCount(0);
            
            SyncBuildinsCount(buildings.Count());

            for (int i = 0; i < _buildingViews.Count; i++)
            {
                _buildingViews[i].SetBuilding(buildings.ElementAt(i));
            }
        }

        private void RequestBuild(string id)
        {
            BuildRequestedSignal.Dispatch(id);
        }

        private void SyncBuildinsCount(int newBuildingsCount)
        {
            while (_buildingViews.Count != newBuildingsCount)
            {
                if (_buildingViews.Count > newBuildingsCount)
                {
                    IBuildingItemView buildingItemView = _buildingViews.First();
                    _buildingViews.RemoveAt(0);
                    
                    buildingItemView.BuildRequestedSignal.RemoveListener(RequestBuild);
                    
                    Destroy(((MonoBehaviour)buildingItemView).gameObject);
                }

                if (_buildingViews.Count < newBuildingsCount)
                {
                    IBuildingItemView buildingItemView = CreateBuildingInstance();
                    ((MonoBehaviour)buildingItemView).transform.SetParent(_itemsRoot, false);
                    
                    buildingItemView.BuildRequestedSignal.AddListener(RequestBuild);
                    
                    _buildingViews.Add(buildingItemView);
                }
            }
        }

        private IBuildingItemView CreateBuildingInstance()
        {
            GameObject instance = Instantiate(_itemPrototype);

            IBuildingItemView buildingItemView = instance.GetComponent<IBuildingItemView>();
            buildingItemView.Init();
            
            return buildingItemView;
        }

        public void OnCloseClicked()
        {
            CloseSignal.Dispatch();
        }
    }
}