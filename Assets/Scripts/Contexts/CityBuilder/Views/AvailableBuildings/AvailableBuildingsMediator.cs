﻿using Company.Contexts.CityBuilder;
using strange.extensions.mediation.impl;

namespace Company.Contexts.CityBuilder
{
    public class AvailableBuildingsMediator : Mediator
    {
        [Inject]
        public AvailableBuildingsView View { get; set; }
        
        [Inject]
        public ICityBuilderModel Model { get; set; }
        
        [Inject]
        public BuildingsUpdatedSignal BuildingsUpdatedSignal { get; set; }
        
        [Inject]
        public ProgressUpdatedSignal ProgressUpdatedSignal { get; set; }
        
        [Inject]
        public BuildRequestedSignal BuildRequestedSignal { get; set; }
        
        [Inject]
        public ActivateBuildingSignal ActivateBuildingSignal { get; set; }
        
        [Inject]
        public BuildBuildingConfirmedSignal ConfirmedSignal { get; set; }
        
        public override void OnRegister()
        {
            View.Init();
            
            View.BuildRequestedSignal.AddListener(BuildBuilding);
            View.CloseSignal.AddListener(CloseView);
            
            BuildingsUpdatedSignal.AddListener(UpdateView);
            ProgressUpdatedSignal.AddListener(UpdateView);
            
            ActivateBuildingSignal.AddListener(ActivateBuilding);
            
            ConfirmedSignal.AddListener(OnConfirmed);
        }

        public override void OnRemove()
        {
            View.BuildRequestedSignal.RemoveListener(BuildBuilding);
            View.CloseSignal.RemoveListener(CloseView);
            
            BuildingsUpdatedSignal.RemoveListener(UpdateView);
            ProgressUpdatedSignal.RemoveListener(UpdateView);
            
            ActivateBuildingSignal.RemoveListener(ActivateBuilding);
            
            ConfirmedSignal.RemoveListener(OnConfirmed);
        }

        private void OnConfirmed(string obj)
        {
            CloseView();
        }

        private void BuildBuilding(string id)
        {
            BuildRequestedSignal.Dispatch(id);
        }

        private void UpdateView()
        {
            View.SetBuildings(Model.Buildings);
        }

        private void ActivateBuilding(string id)
        {
            if (View.AssociatedBuildingId == id)
                gameObject.SetActive(true);
        }

        private void CloseView()
        {
            View.gameObject.SetActive(false);
        }
    }
}