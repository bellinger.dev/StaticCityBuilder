﻿using Company.Contexts.CityBuilder;
using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Company.Contexts.CityBuilder
{
    public class BuildingItemView : View, IBuildingItemView
    {
        [SerializeField] private Image _icon;
        [SerializeField] private Text _title;
        [SerializeField] private Text _contentText;
        [SerializeField] private Button _buildButton;

        private string _id;

        public BuildRequestedSignal BuildRequestedSignal { get; private set; }

        public void Init()
        {
            BuildRequestedSignal = new BuildRequestedSignal();
        }

        public void SetBuilding(IBuilding building)
        {
            _id = building.Id;
            
            _title.text = building.Name;
            _contentText.text = building.Description;

            _buildButton.interactable = !building.IsBuilded;
        }

        public void OnBuildClicked()
        {
            BuildRequestedSignal.Dispatch(_id);
        }
    }
}