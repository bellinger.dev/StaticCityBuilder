﻿using Company.Contexts.CityBuilder;

namespace Company.Contexts.CityBuilder
{
    public interface IBuildingItemView
    {
        BuildRequestedSignal BuildRequestedSignal { get; }

        void Init();
        void SetBuilding(IBuilding building);
    }
}