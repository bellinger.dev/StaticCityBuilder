﻿using strange.extensions.mediation.impl;
using UnityEngine;
using UnityEngine.UI;

namespace Company.Contexts.CityBuilder
{
    public class BuildConfirmationView : View, IBuildConfirmationView
    {
        public BuildConfirmedSignal ConfirmedSignal { get; private set; }
        public BuildCanceledSignal CanceledSignal { get; private set; }

        [SerializeField] private Text _contentText;

        private string _id;
        
        public void Init(string id, string name, string description)
        {
            if (ConfirmedSignal == null)
                ConfirmedSignal = new BuildConfirmedSignal();
            
            if (CanceledSignal == null)
                CanceledSignal = new BuildCanceledSignal();

            _id = id;
            _contentText.text = description;
        }

        public void OnConfirmClicked()
        {
            ConfirmedSignal.Dispatch(_id);
        }

        public void OnRefuseClicked()
        {
            CanceledSignal.Dispatch();
        }
    }
}