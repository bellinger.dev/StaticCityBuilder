﻿using System.Linq;
using strange.extensions.mediation.impl;

namespace Company.Contexts.CityBuilder
{
    public class BuildConfirmationMediator : Mediator
    {
        [Inject]
        public BuildConfirmationView View { get; set; }
        
        [Inject]
        public ICityBuilderModel Model { get; set; }
        
        [Inject]
        public BuildRequestedSignal BuildRequestedSignal { get; set; }
        
        [Inject]
        public CheckBuildingAvailabilitySignal CheckBuildingAvailabilitySignal { get; set; }
        
        [Inject]
        public BuildBuildingConfirmedSignal ConfirmedSignal { get; set; }
        
        [Inject]
        public BuildBuildingRefusedSignal RefusedSignal { get; set; }
        
        public override void OnRegister()
        {
            View.Init(string.Empty, string.Empty, string.Empty);
            
            View.ConfirmedSignal.AddListener(ConfirmBuild);
            View.CanceledSignal.AddListener(CloseView);
            
            BuildRequestedSignal.AddListener(BindView);
            
            ConfirmedSignal.AddListener(OnConfirmed);
            RefusedSignal.AddListener(OnRefused);
        }

        private void OnConfirmed(string id)
        {
            CloseView();
        }

        private void OnRefused(string id)
        {
            CloseView();
        }

        public override void OnRemove()
        {
            View.ConfirmedSignal.RemoveListener(ConfirmBuild);
            View.CanceledSignal.RemoveListener(CloseView);
            
            BuildRequestedSignal.RemoveListener(BindView);
            
            ConfirmedSignal.RemoveListener(OnConfirmed);
            RefusedSignal.RemoveListener(OnRefused);
        }

        private void ConfirmBuild(string id)
        {
            CheckBuildingAvailabilitySignal.Dispatch(id);
        }

        private void BindView(string id)
        {
            IBuilding building = Model.Buildings.Single(b => b.Id == id);
            
            View.Init(building.Id, building.Name, building.Description);
            View.gameObject.SetActive(true);
        }

        private void CloseView()
        {
            gameObject.SetActive(false);
        }
    }
}