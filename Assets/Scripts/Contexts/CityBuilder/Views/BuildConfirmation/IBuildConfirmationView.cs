﻿namespace Company.Contexts.CityBuilder
{
    public interface IBuildConfirmationView
    {
        BuildConfirmedSignal ConfirmedSignal { get; }
        BuildCanceledSignal CanceledSignal { get; }
        
        void Init(string id, string name, string description);
    }
}